import argparse
import pathlib
import yaml
import json

import os, sys
import numpy as np

def main():
    #DEFAULT_TOLERANCE = np.sqrt(sys.float_info.epsilon)
    DEFAULT_TOLERANCE = (sys.float_info.epsilon)**(1.0/3.0)
    parser = argparse.ArgumentParser()
    parser.add_argument('inputyaml', type=argparse.FileType('r'))
    parser.add_argument('--tolerance', type=float, default=DEFAULT_TOLERANCE)
    args = parser.parse_args()
    param = yaml.load(args.inputyaml)
    tolerance = args.tolerance
    try:
        outpath = pathlib.Path(param['OutPath'])
        result_filepath = outpath / 'result.json'
        maxdiff = 100.0

        with result_filepath.open('r', encoding='utf-8') as inputfile:
            data = json.load(inputfile)

        if data:
            maxdiff_t = data['MaxDiff_t']
            maxdiff_rho = data['MaxDiff_rho']
            if np.abs(maxdiff_t) < tolerance and np.abs(maxdiff_rho) < tolerance:
                sys.exit(0)
        sys.exit(1)
    except FileNotFoundError as e:
        sys.exit(1)

            

if __name__=='__main__':
    main()
