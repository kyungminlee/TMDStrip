#!/usr/bin/env python

import sys
import os
from pathlib import Path
import argparse
import copy
import itertools

import yaml
import json
import numpy as np
import scipy as sp

HFB_OBJECTS = ['FractCoord', 'UnitCell',
               'HoppingDiagonal', 'HoppingOffdiagonal',
               'InteractionDiagonal', 'InteractionOffdiagonal',
               'FullHamiltonian', 'HFBSolver', 'HFBComputer']

def jsonconv(obj):
    if isinstance(obj, int):
        return obj
    elif isinstance(obj, float):
        return obj
    elif isinstance(obj, str):
        return obj
    elif isinstance(obj, list):
        return [jsonconv(o) for o in obj]
    elif isinstance(obj, tuple):
        return tuple(jsonconv(o) for o in obj)
    elif isinstance(obj, dict):
        if 'type' in obj:
            if obj['type'] == 'Symbol':
                return obj['value']
            elif obj['type'] == 'Tuple':
                return tuple(jsonconv(obj['value']))
            elif obj['type'] in HFB_OBJECTS:
                return {k: jsonconv(v) for k,v in obj.items()}
            else:
                print(obj['type'])
                print(obj.keys())
                raise TypeError("HA")
        elif 'real' in obj and 'imag' in obj:
            return complex(obj['real'], obj['imag'])
        elif 're' in obj and 'im' in obj:
            return complex(obj['re'], obj['im'])
        else:
            return {k: jsonconv(v) for k,v in obj.items()}
    else:
        return obj



def spin_filter(dirname):
    directory = Path(dirname)
    with (directory / 'hamiltonian.json').open('r') as f:
        hamiltonian = json.load(f)
        hamiltonian = jsonconv(hamiltonian)
    with (directory / 'hfbcomputer.json').open('r') as f:
        hfbcomputer = json.load(f)
        hfbcomputer = jsonconv(hfbcomputer)
    unitcell = hamiltonian['unitcell']
    hoppings = hamiltonian['hoppings']
    latticevectors = np.array(unitcell['latticevectors'])
    orbitals = unitcell['orbitals']

    with (directory / 'result.json').open('r') as f:
        result = json.load(f)

    result_esp = copy.deepcopy(result)
    result_osp = copy.deepcopy(result)

    for idx, (isdiag, i, j, *_) in enumerate(hfbcomputer['t_registry']):
        row_orbname = orbitals[i-1][0]
        col_orbname = orbitals[j-1][0]
        
        if row_orbname[-1] == col_orbname[-1]:
            result_osp['t'][idx] = {'re':0.0, 'im': 0.0}
        else:
            result_esp['t'][idx] = {'re':0.0, 'im': 0.0}

    for idx, (isdiag, i, j, vec, srcs) in enumerate(hfbcomputer['Delta_registry']):
        row_orbname = orbitals[i-1][0]
        col_orbname = orbitals[j-1][0]
        
        if row_orbname[-1] == col_orbname[-1]:
            result_osp['Delta'][idx] = {'re':0.0, 'im': 0.0}
        else:
            result_esp['Delta'][idx] = {'re':0.0, 'im': 0.0}

    with (directory / 'result-esp.json').open('w') as f:
        json.dump(result_esp, f)
    with (directory / 'result-osp.json').open('w') as f:
        json.dump(result_osp, f)

def main():
    parser = argparse.ArgumentParser('spinfilter')
    parser.add_argument('dirnames', type=str, nargs='+')
    args = parser.parse_args()
    for dirname in args.dirnames:
        try:
            spin_filter(dirname)
            print('Spin-filtered {}'.format(dirname))
        except FileNotFoundError as e:
            print("Some files missing. Skipping {}".format(dirname))

if __name__=='__main__':
    main()
