using JSON
using DataStructures
using ArgParse
import YAML
using MicroLogging

using ProgressMeter
using HartreeFockBogoliubov
import HartreeFockBogoliubov: Spec, Generator, HFB
using HartreeFockBogoliubov: HFB


function detectresult(outpath::AbstractString)
  for resultfilename in ["result.json",]
    result = nothing
    resultfilepath = joinpath(outpath, resultfilename)
    try
      if isfile(resultfilepath)
        open(resultfilepath) do file
          result = JSON.parse(file)
        end
      end
    catch y
      result = nothing
      #@show typeof(y)
      #if isa(y, LoadError)
      #else
      #  println("Duh")
      #  rethrow(y)
      #end
    end

    if result != nothing
      ρ = [ float(x["re"]) + 1im * float(x["im"]) for x in result["rho"] ]
      t = [ float(x["re"]) + 1im * float(x["im"]) for x in result["t"] ]
      Γ = [ float(x["re"]) + 1im * float(x["im"]) for x in result["Gamma"] ]
      Δ = [ float(x["re"]) + 1im * float(x["im"]) for x in result["Delta"] ]
      return (result["BatchRun"], HFBSolution(ρ,t,Γ,Δ))
    end
  end

  #=
  for resultfilename in ["result_concise.yaml", "result_history.yaml"]
    result = nothing
    resultfilepath = joinpath(outpath, resultfilename)
    if isfile(resultfilepath)
      open(resultfilepath) do file
        prev_yaml = YAML.load_all(file)
        for s in prev_yaml
          result = s
        end
      end
    end

    if result != nothing
      ρ = [ float(x["real"]) + 1im * float(x["imag"]) for x in result["rho"] ]
      t = [ float(x["real"]) + 1im * float(x["imag"]) for x in result["t"] ]
      Γ = [ float(x["real"]) + 1im * float(x["imag"]) for x in result["Gamma"] ]
      Δ = [ float(x["real"]) + 1im * float(x["imag"]) for x in result["Delta"] ]
      return HFBSolution(ρ,t,Γ,Δ)
    end
  end
  =#
  return (0, nothing)
end

function runloop(solver ::HFBSolver;
                 outpath ::AbstractString="out",
                 nwarmup ::Integer = 200,
                 nbunch  ::Integer = 100,
                 nbatch  ::Integer = 500,
                 tolerance::Float64 = sqrt(eps(Float64)),
                 noise::Real = 0,
                 verbose::Bool=false,
                 update::Function=simpleupdate)

  @info "Entering runloop"
  progressbar = verbose && haskey(ENV, "TERM") && (ENV["TERM"] != "dumb")

  @assert(nwarmup >= 0)
  @assert(nbunch > 0)
  @assert(nbatch >= 0)

  @info "Making path $(outpath)"
  mkpath(outpath)

  hamspec = solver.hamiltonian
  uc = hamspec.unitcell

  @info "Making solution"
  currentsolution = newhfbsolution(solver.hfbcomputer)

  @info "Detecting result"
  batchrun_offset, lastsolution = detectresult(outpath)

  if lastsolution != nothing
    if iscompatible(currentsolution, lastsolution)
      @info("Previous Solution matches in size with current solution. Let's use it.")
      currentsolution = lastsolution
      @info ("Successfully read.")

      if verbose
        !isempty(currentsolution.ρ) && println("  maximum |rho|  : ", maximum(abs.(currentsolution.ρ)))
        !isempty(currentsolution.t) && println("  maximum |t|    : ", maximum(abs.(currentsolution.t)))
        !isempty(currentsolution.Γ) && println("  maximum |Gamma|: ", maximum(abs.(currentsolution.Γ)))
        !isempty(currentsolution.Δ) && println("  maximum |Delta|: ", maximum(abs.(currentsolution.Δ)))
      end

      if abs(noise) > 0
        @info("Adding noise to the values")
        currentsolution.ρ[:] += noise * (2*rand(Float64, size(currentsolution.ρ)) - 1)
        currentsolution.t[:] += noise * (2*rand(Complex128, size(currentsolution.t)) - 1 - 1im)
        currentsolution.Γ[:] += noise * (2*rand(Float64, size(currentsolution.Γ)) - 1)
        currentsolution.Δ[:] += noise * (2*rand(Complex128, size(currentsolution.Δ)) - 1 - 1im)
      end
    else
      @warn ("Previous solution has different size from current solution")
      @warn ("Something is wrong. Just using the new solution")
      batchrun_offset = 0
      randomize!(solver.hfbcomputer, currentsolution)
    end
  else
    @info "Result not found"
    batchrun_offset = 0
    randomize!(solver.hfbcomputer, currentsolution)
  end

  verbose && @info("Benchmarking")

  time_julia = median([
    let
      start_time = now()
      for i in 1:4
        getnextsolution(solver, currentsolution)
      end
      end_time = now()
      float(Dates.value(end_time - start_time))
    end
    for i in 1:5
  ]) / 4

  time_python = median([
    let
      start_time = now()
      for i in 1:4
        getnextsolutionpython(solver, currentsolution)
      end
      end_time = now()
      float(Dates.value(end_time - start_time))
    end
    for i in 1:5
  ]) / 4

  if verbose
    @info("Julia : $(time_julia)ms")
    @info("Python: $(time_python)ms")
  end

  getnextsolutionbest, loopbest = if time_python < time_julia
    verbose && @info("Choosing Python!")
    getnextsolutionpython, looppython
  else
    verbose && @info("Choosing Julia!")
    getnextsolution, loop
  end

  if batchrun_offset == 0
    @info("Warm Up")
    if progressbar
      @showprogress for run in 1:nwarmup
        currentsolution = getnextsolutionbest(solver, currentsolution)
        currentsolution.Γ[:] = 0
      end
    else
      for run in 1:nwarmup
        currentsolution = getnextsolutionbest(solver, currentsolution)
        currentsolution.Γ[:] = 0
      end
    end
  end

  for batchrun in 1:nbatch
    verbose && println("BATCH $(batchrun + batchrun_offset)")
    callback = if progressbar
        p = Progress(nbunch)
        (i, n) -> next!(p)
    else
        (i, n) -> nothing
    end

    starttime = now()
    previoussolution = copy(currentsolution)
    currentsolution = loopbest(solver, currentsolution, nbunch;
                               update=update, callback=callback)
    endtime = now()
    verbose && println("Duration: ", (endtime - starttime))

    maxdiff1 = isempty(currentsolution.ρ) ? 0.0 : maximum(abs.(currentsolution.ρ - previoussolution.ρ))
    maxdiff2 = isempty(currentsolution.t) ? 0.0 : maximum(abs.(currentsolution.t - previoussolution.t))
    maxdiff = max(maxdiff1, maxdiff2)

    if verbose
      println("MaxDiff_rho = $maxdiff1")
      println("MaxDiff_t   = $maxdiff2")
      #println("MaxDiff     = $maxdiff")
    end

    #=
    open(joinpath(outpath, "result_history.yaml"), "a") do file
      FMT(x...) = begin
        foreach(z -> mydump(file, z), x)
        println(file)
      end
      FMT("---")
      FMT("BatchRun: ",    batchrun)
      FMT("rho: ",         currentsolution.ρ)
      FMT("t: ",           currentsolution.t)
      FMT("Gamma: ",       currentsolution.Γ)
      FMT("Delta: ",       currentsolution.Δ)
      FMT("MaxDiff_rho: ", maxdiff1)
      FMT("MaxDiff_t: ",   maxdiff2)
      FMT("StartTime: ",   Dates.format(starttime, "yyyy-mm-ddTHH:MM:SS.s"))
      FMT("EndTime: ",     Dates.format(endtime, "yyyy-mm-ddTHH:MM:SS.s"))
      FMT("...")
    end

    open(joinpath(outpath, "result_concise.yaml"), "w") do file
      FMT(x...) = begin
        foreach(z -> mydump(file, z), x)
        println(file)
      end
      FMT("---")
      FMT("BatchRun: ",    batchrun)
      FMT("rho: ",         currentsolution.ρ)
      FMT("t: ",           currentsolution.t)
      FMT("Gamma: ",       currentsolution.Γ)
      FMT("Delta: ",       currentsolution.Δ)
      FMT("MaxDiff_rho: ", maxdiff1)
      FMT("MaxDiff_t: ",   maxdiff2)
      FMT("StartTime: ",   Dates.format(starttime, "yyyy-mm-ddTHH:MM:SS.s"))
      FMT("EndTime: ",     Dates.format(endtime, "yyyy-mm-ddTHH:MM:SS.s"))
      FMT("...")
    end
    =#

    if isfile(joinpath(outpath, "result.json"))
      mv(joinpath(outpath, "result.json"),
         joinpath(outpath, "previous_result.json"),
         remove_destination=true)
    end
    open(joinpath(outpath, "result.json"), "w") do file
      outdict = OrderedDict([
        ("BatchRun",    batchrun_offset + batchrun),
        ("rho",         currentsolution.ρ),
        ("t",           currentsolution.t),
        ("Gamma",       currentsolution.Γ),
        ("Delta",       currentsolution.Δ),
        ("MaxDiff_rho", maxdiff1),
        ("MaxDiff_t",   maxdiff2),
        ("StartTime",   Dates.format(starttime, "yyyy-mm-ddTHH:MM:SS.s")),
        ("EndTime",     Dates.format(endtime, "yyyy-mm-ddTHH:MM:SS.s")),
      ])
      JSON.print(file, outdict)
    end

    if maxdiff < tolerance
      return
    end

  end
end
