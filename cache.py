
import sys, os
import time
import re
import itertools
import argparse
import pickle

import numpy as np
import numpy.linalg as npl
import scipy as sp
import scipy.interpolate

import yaml
import progressbar


hexpat = re.compile(r"[+-]?0x([0-9a-fA-F]+(\.[0-9a-f]*)?|(\.[0-9a-f]*))(p[+-]?[0-9a-f]+)?",
                    re.IGNORECASE)

def conv(obj):
    if isinstance(obj, dict):
        if set(obj.keys()) == set(['real', 'imag']):
            return complex(conv(obj['real']), conv(obj['imag']))
        else:
            return {k: conv(v) for (k,v) in obj.items()}
    elif isinstance(obj, list):
        return [conv(v) for v in obj]
    elif isinstance(obj, int):
        return obj
    elif isinstance(obj, float):
        return obj
    elif isinstance(obj, str):
        if hexpat.match(obj):
            return float.fromhex(obj)
        else:
            return obj
    else:
        return obj


def main():
    params = dict()
    solutions = dict()

    parser = argparse.ArgumentParser()
    parser.add_argument('rootdir', type=str)
    args = parser.parse_args()

    rootdir = args.rootdir
    dirnames = [dirname for dirname in os.listdir(rootdir) if os.path.isdir(os.path.join(rootdir, dirname))]

    bar = progressbar.ProgressBar()

    def wrappath(*path):
        return os.path.join(rootdir, dirname, *path)

    for dirname in bar(dirnames):
        try:
            param = yaml.load(open(os.path.join(rootdir, dirname, 'parameters.yaml'), "r", encoding="utf-8"))
            param = conv(param)
            solution = None
            if os.path.exists(os.path.join(rootdir, dirname, "result_concise.yaml")):
                for solution in yaml.load_all(open(os.path.join(rootdir, dirname, "result_concise.yaml"), "r", encoding="utf-8")):
                    pass
        
            #READ RESULT IF CONCISE FAILED.
            if solution is None:
                for solution in yaml.load_all(open(os.path.join(rootdir, dirname, "result_history.yaml"), "r", encoding="utf-8")):
                    pass

            if solution is None:
                continue
            
            solution = conv(solution)

            params[dirname] = param
            solutions[dirname] = solution
        except FileNotFoundError as e:
            pass

    print("Read {} params and {} solutions".format(len(params), len(solutions)))

    with open(os.path.join(rootdir, 'params.cache.pickle'), 'wb') as outputfile:
        pickle.dump(params, outputfile)
    with open(os.path.join(rootdir, 'solutions.cache.pickle'), 'wb') as outputfile:
        pickle.dump(solutions, outputfile)

if __name__=='__main__':
    main()
