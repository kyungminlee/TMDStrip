using JSON
using DataStructures
using ArgParse
import YAML

using ProgressMeter
using HartreeFockBogoliubov
import HartreeFockBogoliubov: Spec, Generator, HFB
using HartreeFockBogoliubov: HFB
using HartreeFockBogoliubov: Dictify

using MicroLogging

using PyCall

@pyimport numpy.linalg as npl

include("./HFBLooper.jl")
include("./TMDStripHamiltonian.jl")

function runstripeigen(
  size ::Tuple{<:Integer, <:Integer},
  supersize ::Tuple{<:Integer, <:Integer},
  periodic ::Tuple{Bool, Bool};

  μ ::Real=0.0,
  t ::Real=1.0,
  mAB ::Real=0.0,
  λIsing ::Real=0.0,
  U ::Real=0.0,
  V ::Real=0.0,
  temperature ::Real=0.0,

  outpath::AbstractString="out",
  verbose::Bool=true)

  progressbar = verbose && haskey(ENV, "TERM") && (ENV["TERM"] != "dumb")

  m1, m2 = supersize

  hamspec = makestriphamiltonian(size, periodic,
                                 float(μ), float(t), float(mAB), float(λIsing), float(U), float(V))
  solver = HFBSolver(hamspec, [m1, m2], float(temperature))
  uc = hamspec.unitcell

  (batchrun, currentsolution) = detectresult(outpath)
  if currentsolution == nothing
    @error "Bad solution. Quitting"
  else
    Γ = currentsolution.Γ
    Δ = currentsolution.Δ
    @info "Succesfully read solution"
    if isempty(Γ)
      @info "Γ: empty"
    else
      @info "Γ: len=$(length(Γ)), max=$(maximum(abs.(Γ)))"
    end
    if isempty(Δ)
      @info "Δ: empty"
    else
      @info "Δ: len=$(length(Δ)), max=$(maximum(abs.(Δ)))"
    end
  end

  currentsolution.Γ[:] = 0
  hamiltonian = makehamiltonian(solver.hfbcomputer,
                                currentsolution.Γ,
                                currentsolution.Δ)

  progresscount = if progressbar
    p = Progress(length(solver.momentumgrid))
    () -> next!(p)
  else
    () -> nothing
  end

  if verbose
    @info "Computing eigenvalues and eigenvectors"
  end
  all_eigenvalues = Vector{Float64}[]
  all_eigenvectors = Matrix{Complex128}[]
  #minimum_eigenvalue = Inf64

  for momentum in solver.momentumgrid
    hk = hamiltonian(momentum)
    #(eigenvalues, eigenvectors) = eig(Hermitian(hk))
    (eigenvalues, eigenvectors) = npl.eigh(hk)
    push!(all_eigenvalues, eigenvalues)
    push!(all_eigenvectors, eigenvectors)
    #minimum_eigenvalue = min(minimum_eigenvalue, minimum(abs.(eigenvalues)))
    progresscount()
  end

  if verbose
    @info "Saving results"
  end
  open(joinpath(outpath, "eigenvalues.json"), "w") do file
    JSON.print(file, OrderedDict([("BatchRun", batchrun),
                                  ("Momentums", solver.momentumgrid),
                                  ("Eigenvalues", all_eigenvalues),
                                  ]))
  end

  #=
  open(joinpath(outpath, "eigenvectors.json"), "w") do file
    JSON.print(file, OrderedDict([("BatchRun", batchrun),
                                  ("Momentums", solver.momentumgrid),
                                  ("Eigenvalues", all_eigenvalues),
                                  ("Eigenvectors", all_eigenvectors),]))
  end
  =#
end



function parse_commandline()
  s = ArgParseSettings()
  @add_arg_table s begin
    "infilename"
    help = "Input Filename"
    required = true
    "--ratio"
    arg_type = Int
    default = 1
    "--verbose", "-v"
    action = :store_true
  end
  return parse_args(s)
end

function main()
  args = parse_commandline()

  ratio = args["ratio"]
  @assert(ratio > 0)

  infilename = args["infilename"]
  @info "Reading $infilename"

  parameters = YAML.load(open(args["infilename"]))

  boundarytype = parameters["BoundaryType"]
  outpath = parameters["OutPath"]
  n1    = Int(parameters["n1"])
  n2    = Int(parameters["n2"])

  μ           = Float64(parameters["ChemicalPotential"])
  t           = Float64(parameters["NearestNeighborHopping"])
  mAB         = Float64(parameters["ChargeTransferEnergy"])
  λIsing      = Float64(parameters["IsingSpinOrbitCoupling"])
  U           = Float64(parameters["OnSiteInteraction"])
  V           = Float64(parameters["NearestNeighborInteraction"])
  temperature = Float64(parameters["Temperature"])

  seed = Int(parameters["Seed"])

  srand(seed)

  if boundarytype == "Zigzag"
    supersize = n1 * ratio, 1
    size = 3, n2
    periodic = (true, false)
  elseif boundarytype == "Armchair"
    supersize = 1, n2 * ratio
    size = n1, 1
    periodic = (false, true)
  else
    @error "BoundaryType $boundarytype not supported."
  end

  if mod(size[1], 3) != 0
    warn("n1 not multiple of 3 (incommensurate with K/K' wave vector)")
  end

  runstripeigen(size, supersize, periodic;
           μ=μ,
           t=t,
           mAB=mAB,
           λIsing=λIsing,
           U=U,
           V=V,
           temperature=temperature,
           outpath=outpath,
           verbose=args["verbose"])
  #end
end

main()
