using JSON
using DataStructures
using ArgParse
import YAML
using MicroLogging

using ProgressMeter
using HartreeFockBogoliubov
import HartreeFockBogoliubov: Spec, Generator, HFB
using HartreeFockBogoliubov: HFB
using HartreeFockBogoliubov: Dictify

include("./HFBLooper.jl")
include("./TMDStripHamiltonian.jl")

function runstrip(
  size ::Tuple{<:Integer, <:Integer},
  supersize ::Tuple{<:Integer, <:Integer},
  periodic ::Tuple{Bool, Bool};

  μ ::Real=0.0,
  t ::Real=1.0,
  mAB ::Real=0.0,
  λIsing ::Real=0.0,
  U ::Real=0.0,
  V ::Real=0.0,
  temperature ::Real=0.0,
  nwarmup ::Integer=100,
  nbunch ::Integer=100,
  nbatch ::Integer=1000,
  noise::Real=0,

  outpath::AbstractString="out",
  verbose::Bool=false)

  @info "Entering runstrip"

  if nwarmup < 0
    throw(ArgumentError("nwarmup should be non-negative"))
  end
  if nbunch <= 0
    throw(ArgumentError("nbunch should be positive"))
  end
  if nbatch <= 0
    throw(ArgumentError("nbatch should be non-negative"))
  end

  m1, m2 = supersize

  @info "Making Hamiltonian"
  hamspec = makestriphamiltonian(size, periodic,
                                 float(μ), float(t), float(mAB), float(λIsing), float(U), float(V))
  @info "Making solver"
  solver = HFBSolver(hamspec, [m1, m2], float(temperature))
  uc = hamspec.unitcell

  @info "Making $outpath"
  mkpath(outpath)

  @info "Saving parameters"
  open(joinpath(outpath, "parameters.json"), "w") do file
    outputParameters = OrderedDict{String, Any}([
      ("ChemicalPotential", μ),
      ("NearestNeighborHopping", t),
      ("ChargeTransferEnergy", mAB),
      ("IsingSpinOrbitCoupling", λIsing),
      ("OnSiteInteraction", U),
      ("NearestNeighborInteraction", V),
      ("Temperature", temperature),

      ("Size", size),
      ("SuperSize", supersize),
      ("Periodic", periodic),
    ])
    JSON.print(file, outputParameters)
  end

  @info "Saving hamiltonian"
  open(joinpath(outpath, "hamiltonian.json"), "w") do file
    hamdict = dictify(hamspec)
    JSON.print(file, hamdict)
  end

  @info "Saving momentumgrid"
  open(joinpath(outpath, "momentumgrid.json"), "w") do file
    JSON.print(file, solver.momentumgrid)
  end

  @info "Saving hfbsolver"
  open(joinpath(outpath, "hfbsolver.json"), "w") do file
    hamdict = dictify(solver)
    JSON.print(file, hamdict)
  end

  @info "Saving hfbcomputer"
  open(joinpath(outpath, "hfbcomputer.json"), "w") do file
    hamdict = dictify(solver.hfbcomputer)
    JSON.print(file, hamdict)
  end

  function nogammaupdate(sol::HFBSolution, newsol::HFBSolution)
    simpleupdate(sol, newsol)
    sol.Γ[:] = 0
    sol
  end

  @info "Starting loop"
  runloop(solver;
          outpath=outpath,
          nwarmup=nwarmup,
          nbunch=nbunch,
          nbatch=nbatch,
          verbose=verbose,
          update=nogammaupdate,
          noise=noise,
          tolerance=(eps(Float64))^(1.0/3.0),
          )
end



function parse_commandline()
  s = ArgParseSettings()
  @add_arg_table s begin
    "infilename"
    help = "Input Filename"
    required = true
    #=
    "--eigenvalues"
    help = "Compute Eigenvalues"
    action = :store_true
    "--eigenvectors"
    help = "Compute Eigenvectors"
    action = :store_true
    "--bare"
    help = "Compute Bare Bandstructure"
    action = :store_true
    =#
    "--nwarmup"
    arg_type = Int
    default = 100
    "--nbunch"
    arg_type = Int
    default = 100
    "--nbatch"
    arg_type = Int
    default = 1000
    "--noise"
    arg_type = Float64
    default = 0.0

    "--verbose", "-v"
    action = :store_true
    #"--m1"
    #arg_type = Int
    #default = 0
    #"--m2"
    #arg_type = Int
    #default = 0
  end
  return parse_args(s)
end

function main()
  args = parse_commandline()

  nwarmup = args["nwarmup"]
  nbunch = args["nbunch"]
  nbatch = args["nbatch"]

  parameters = YAML.load(open(args["infilename"]))

  boundarytype = parameters["BoundaryType"]
  outpath = parameters["OutPath"]
  n1    = Int(parameters["n1"])
  n2    = Int(parameters["n2"])

  μ           = Float64(parameters["ChemicalPotential"])
  t           = Float64(parameters["NearestNeighborHopping"])
  mAB         = Float64(parameters["ChargeTransferEnergy"])
  λIsing      = Float64(parameters["IsingSpinOrbitCoupling"])
  U           = Float64(parameters["OnSiteInteraction"])
  V           = Float64(parameters["NearestNeighborInteraction"])
  temperature = Float64(parameters["Temperature"])

  seed = Int(parameters["Seed"])

  srand(seed)
  #m1 = (args["m1"] > 0) ? args["m1"] : m1
  #m2 = (args["m2"] > 0) ? args["m2"] : m2

  if boundarytype == "Zigzag"
    supersize = n1, 1
    size = 3, n2
    periodic = (true, false)
  elseif boundarytype == "Armchair"
    supersize = 1, n2
    size = n1, 1
    periodic = (false, true)
  else
    println("BoundaryType $boundarytype not supported.")
    exit(1)
  end

  if mod(size[1], 3) != 0
    warn("n1 not multiple of 3 (incommensurate with K/K' wave vector)")
  end
  #=
  if args["bare"]
    bareeigenarmchair(n1, n2, width;
           μ=μ,
           t=t,
           mAB=mAB,
           λIsing=λIsing,
           outpath=outpath,
           verbose=args["verbose"])
  elseif args["eigenvalues"] || args["eigenvectors"]
    eigenarmchair(n1, n2, width;
           μ=μ,
           t=t,
           mAB=mAB,
           λIsing=λIsing,
           U=U,
           V=V,
           temperature=temperature,
           compute_eigenvectors=args["eigenvectors"],
           outpath=outpath,
           verbose=args["verbose"])

  else
  =#
  runstrip(size, supersize, periodic;
           μ=μ,
           t=t,
           mAB=mAB,
           λIsing=λIsing,
           U=U,
           V=V,
           temperature=temperature,
           nwarmup=nwarmup,
           nbunch=nbunch,
           nbatch=nbatch,
           noise=args["noise"],
           outpath=outpath,
           verbose=args["verbose"])
  #end
end

main()
