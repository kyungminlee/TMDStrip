import sys
import os
import itertools
import argparse

import numpy as np
import scipy as sp

import yaml
import json

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors
import matplotlib.cm
import cmocean

from matplotlib.backends.backend_pdf import PdfPages
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, zoomed_inset_axes


HFB_OBJECTS = ['FractCoord', 'UnitCell',
               'HoppingDiagonal', 'HoppingOffdiagonal',
               'InteractionDiagonal', 'InteractionOffdiagonal',
               'FullHamiltonian', 'HFBSolver', 'HFBComputer']

def jsonconv(obj):
    if isinstance(obj, int):
        return obj
    elif isinstance(obj, float):
        return obj
    elif isinstance(obj, str):
        return obj
    elif isinstance(obj, list):
        return [jsonconv(o) for o in obj]
    elif isinstance(obj, tuple):
        return tuple(jsonconv(o) for o in obj)
    elif isinstance(obj, dict):
        if 'type' in obj:
            if obj['type'] == 'Symbol':
                return obj['value']
            elif obj['type'] == 'Tuple':
                return tuple(jsonconv(obj['value']))
            elif obj['type'] in HFB_OBJECTS:
                return {k: jsonconv(v) for k,v in obj.items()}
            else:
                print(obj['type'])
                print(obj.keys())
                raise TypeError("HA")
        elif 'real' in obj and 'imag' in obj:
            return complex(obj['real'], obj['imag'])
        elif 're' in obj and 'im' in obj:
            return complex(obj['re'], obj['im'])
        else:
            return {k: jsonconv(v) for k,v in obj.items()}
    else:
        return obj


def plot_pairing_onsite_zigzag(dirname):
    with open(os.path.join(dirname,'hamiltonian.json'), 'r') as f:
        hamiltonian = json.load(f)
        hamiltonian = jsonconv(hamiltonian)
    with open(os.path.join(dirname,'hfbcomputer.json'), 'r') as f:
        hfbcomputer = json.load(f)
        hfbcomputer = jsonconv(hfbcomputer)
    with open(os.path.join(dirname,'result_concise.yaml'), 'r') as f:
        for result in yaml.load_all(f):
            pass

    Deltas = jsonconv(result['Delta'])
    if Deltas:
        maxDelta = np.max(np.abs(Deltas))
    else:
        Deltas = []
        maxDelta = 0.0

    unitcell = hamiltonian['unitcell']
    hoppings = hamiltonian['hoppings']

    latticevectors = np.array(unitcell['latticevectors'])
    orbitals = unitcell['orbitals']

    def oc(idx):
        fc = orbitals[idx-1][1]
        fc = np.array(fc['whole']) + np.array(fc['fraction'])
        return np.dot(fc, latticevectors)

    upcoords = []
    dncoords = []

    for (orbname, fc) in orbitals:
        r = np.dot( np.array(fc['fraction']) + np.array(fc['whole']), latticevectors)
        if orbname[-1] == 'UP':
            upcoords.append(r)
        else:
            dncoords.append(r)
            
    upcoords = np.array(upcoords)
    dncoords = np.array(dncoords)

    refDelta = 0.5 # Reference Delta
    markerSizeRatio = 15


    TOLERANCE = 1E-6

    fig = plt.figure(figsize=(16,12))
    ax = fig.gca()

    ax.set_aspect(1.0)
    for i1, i2 in itertools.product(range(-6,7),[0]):
        r0 = np.dot([i1,i2], latticevectors)
        ax.plot(r0[0] + upcoords[:,0],
                r0[1] + upcoords[:,1],
                'o',
                zorder=0,
                color='k',
                alpha=0.1,
                markerfacecolor='w', 
                markeredgecolor='k',
            markersize=4)

    for ((isdiag, rowindex, colindex, vec, srcs), Delta) in zip(hfbcomputer['Delta_registry'], Deltas):
        row_orbname, row_fc = orbitals[rowindex-1]
        col_orbname, col_fc = orbitals[colindex-1]
        assert(row_orbname[1] == col_orbname[1])
        if row_orbname[3] == 'UP':
            vec = np.array(vec)
        elif row_orbname[3] == 'DN':
            (rowindex, colindex) = (colindex, rowindex)
            (row_orbname, col_orbname) = (col_orbname, row_orbname)
            (row_fc, col_fc) = (col_fc, row_fc)
            vec = -np.array(vec)
            Delta = -Delta
        
        if np.abs(Delta) < TOLERANCE:
            continue
            
        c = cmocean.cm.phase((np.angle(Delta) / (2*np.pi)) % 1.0)
        alpha = np.abs(Delta) / maxDelta
        for i1, i2 in itertools.product(range(-6,7), [0]):
            r0 = np.dot([i1,i2], latticevectors)
            ri = oc(rowindex) + r0
            assert(np.all(np.isclose(vec, 0)))
            ax.plot([ri[0]], [ri[1]],
                    'o',
                    markersize=np.abs(Delta)/refDelta*markerSizeRatio,
                    color=c,
                    alpha=alpha)

    ax.set_xlim(-4,4)
    ax.set_xticks([])
    ax.set_yticks([])

    fig.savefig(os.path.join(dirname, 'PairingGap.pdf'), dpi=300, bbox_inches='tight')
    fig.savefig(os.path.join(dirname, 'PairingGap.png'), dpi=300, bbox_inches='tight')

    plt.clf()
    plt.close()
    

def plot_eigen_zigzag(dirname):
    with open(os.path.join(dirname,'bare_eigenvalues.json'), 'r') as f:
        eigenvalues_json = json.load(f)

    eigenvalues = np.array(eigenvalues_json['eigenvalues'])
    momentums =  np.array(eigenvalues_json["momentums"])

    ks = momentums[0,:,1]

    fig = plt.figure(figsize=(6,6))
    ax = fig.gca()
    xs = np.hstack([ks, ks + 2*np.pi / 6.0])
    for i in range(eigenvalues.shape[1]):
        ys = np.hstack([eigenvalues[:,i],eigenvalues[:,i]])
        ax.plot(xs, ys, 'k-', linewidth=0.1)
    ax.set_xlabel('$k_y$', fontsize=16)
    ax.set_ylabel('$E$', fontsize=16)
    ax.set_xlim(np.min(xs), np.max(xs))
    ax.set_ylim(-1, 1)

    fig.savefig(os.path.join(dirname, 'BareEigenvalues.pdf'), dpi=300, bbox_inches='tight')
    fig.savefig(os.path.join(dirname, 'BareEigenvalues.png'), dpi=300, bbox_inches='tight')

    plt.clf()
    plt.close()    

def main():
    parser = argparse.ArgumentParser("plotall")
    parser.add_argument("dirnames", nargs='+', type=str)

    args = parser.parse_args()
    
    dirnames = [dirname for dirname in args.dirnames if os.path.isdir(dirname)]
    
    for dirname in dirnames:
        print(dirname)
        plot_pairing_onsite_zigzag(dirname)
        plot_eigen_zigzag(dirname)

if __name__=='__main__':
    main()
