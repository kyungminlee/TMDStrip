using DataStructures
using MicroLogging

using HartreeFockBogoliubov
import HartreeFockBogoliubov: Spec, Generator, HFB
using HartreeFockBogoliubov: HFB

function makestriphamiltonian(
        size :: Tuple{<:Integer, <:Integer},
        periodic ::Tuple{Bool, Bool},
        μ ::Real,
        t ::Real,
        mAB ::Real,
        λIsing ::Real,
        U ::Real,
        V ::Real)
  a0 = [ 0.0, 0.0]
  a1 = [ 0.0, 1.0]
  a2 = [-sqrt(3.0) * 0.5,-0.5]
  a3 = [ sqrt(3.0) * 0.5,-0.5]
  b1 = a3 - a2
  b2 = a1 - a3
  b3 = a2 - a1

  c1 = b1 - b2
  c2 = b2 - b3

  n1 ::Int, n2 ::Int = size
  p1 ::Bool, p2 ::Bool = periodic

  sqA1, sqA2 = b1, a1*3    # lattice vectors of the 4-site unit cell

  unitcell = newunitcell([(sqA1*n1) (sqA2*n2)], OrbitalType=Tuple{Int64, Int64, Symbol, Int64, Symbol})
  r_origin = a1 # location of A1 from which the coordinates of other orbitals are calculated
  orbloc = Dict(
    (:A, 1) => r_origin + a0,
    (:B, 1) => r_origin + a1,
    (:A, 2) => r_origin - b3,
    (:B, 2) => r_origin + a3,
  )

  for sp in [:UP, :DN], idx in [1,2], orb in [:A, :B], i2 in 1:n2, i1 in 1:n1
    R = sqA1 * (i1-1) + sqA2 * (i2-1)
    r = R + orbloc[orb, idx]
    addorbital!(unitcell, (i1, i2, orb, idx, sp), carte2fract(unitcell, r))
  end

  hamspec = Spec.FullHamiltonian(unitcell)

  function addhop(orb1, orb2, dr, value)
    i1, i2 = orb1[1], orb1[2]
    j1, j2 = orb2[1], orb2[2]

    # Wrap around if periodic
    if p1
      i1, j1 = mod1(i1, n1), mod1(j1, n1)
    end
    if p2
      i2, j2 = mod1(i2, n2), mod1(j2, n2)
    end

    orb1 = (i1, i2, orb1[3:end]...)
    orb2 = (j1, j2, orb2[3:end]...)
    if ! hasorbital(unitcell, orb1)
      @warn("orbital $orb1 does not exist")
      return
    end
    if ! hasorbital(unitcell, orb2)
      @warn("orbital $orb2 does not exist")
      return
    end
    r1 = sqA1 * (i1-1) + sqA2 * (i2-1) + orbloc[orb1[3:4]]
    r2 = r1 + dr
    Spec.addhopping!(hamspec, Spec.hoppingbycarte(unitcell, value, orb1, orb2, r1, r2))
  end

  function addint(orb1, orb2, dr, value)
    i1, i2 = orb1[1], orb1[2]
    j1, j2 = orb2[1], orb2[2]

    # Wrap around if periodic
    if p1
      i1, j1 = mod1(i1, n1), mod1(j1, n1)
    end
    if p2
      i2, j2 = mod1(i2, n2), mod1(j2, n2)
    end

    orb1 = (i1, i2, orb1[3:end]...)
    orb2 = (j1, j2, orb2[3:end]...)
    if ! hasorbital(unitcell, orb1)
      warn("orbital $orb1 does not exist")
      return
    end
    if ! hasorbital(unitcell, orb2)
      warn("orbital $orb2 does not exist")
      return
    end
    r1 = sqA1 * (i1-1) + sqA2 * (i2-1) + orbloc[orb1[3:4]]
    r2 = r1 + dr
    Spec.addinteraction!(hamspec, Spec.interactionbycarte(unitcell, value, orb1, orb2, r1, r2))
  end

  #for sp in [:UP, :DN], i1 in 1:n1, i2 in 1:n2, orb in [:A, :B], idx in [1,2]
  for sp in [:UP, :DN], idx in [1,2], orb in [:A, :B], i2 in 1:n2, i1 in 1:n1
    r = sqA1 * (i1-1) + sqA2 * (i2-1) + orbloc[orb, idx]
    val = (orb == :A) ? (-μ + mAB) : (-μ - mAB)
    Spec.addhopping!(hamspec, Spec.hoppingbycarte(unitcell, val, (i1, i2, orb, idx, sp), r))
  end

  #for sp in [:UP, :DN], i1 in 1:n1, i2 in 1:n2
  for sp in [:UP, :DN], i2 in 1:n2, i1 in 1:n1
    addhop((i1, i2, :A, 1, sp), (i1  , i2  , :B, 1, sp), a1, -t)
    addhop((i1, i2, :A, 1, sp), (i1-1, i2  , :B, 2, sp), a2, -t)
    addhop((i1, i2, :A, 1, sp), (i1  , i2  , :B, 2, sp), a3, -t)

    addhop((i1, i2, :A, 2, sp), (i1  , i2+1, :B, 2, sp), a1, -t)
    addhop((i1, i2, :A, 2, sp), (i1  , i2  , :B, 1, sp), a2, -t)
    addhop((i1, i2, :A, 2, sp), (i1+1, i2  , :B, 1, sp), a3, -t)
  end

  if abs(λIsing) > eps(Float64)
    #for sp in [:UP, :DN], i1 in 1:n1, i2 in 1:n2
    for sp in [:UP, :DN], i2 in 1:n2, i1 in 1:n1
      λ = 1im * λIsing * (sp == :UP ? 1 : -1)

      # orbital A
      addhop((i1, i2, :A, 1, sp), (i1+1, i2  , :A, 1, sp), b1, -λ)
      addhop((i1, i2, :A, 1, sp), (i1-1, i2  , :A, 2, sp), b2, -λ)
      addhop((i1, i2, :A, 1, sp), (i1-1, i2-1, :A, 2, sp), b3, -λ)

      addhop((i1, i2, :A, 2, sp), (i1+1, i2  , :A, 2, sp), b1, -λ)
      addhop((i1, i2, :A, 2, sp), (i1  , i2+1, :A, 1, sp), b2, -λ)
      addhop((i1, i2, :A, 2, sp), (i1  , i2  , :A, 1, sp), b3, -λ)

      # orbital B
      addhop((i1, i2, :B, 1, sp), (i1+1, i2  , :B, 1, sp), b1,  λ)
      addhop((i1, i2, :B, 1, sp), (i1-1, i2+1, :B, 2, sp), b2,  λ)
      addhop((i1, i2, :B, 1, sp), (i1-1, i2  , :B, 2, sp), b3,  λ)

      addhop((i1, i2, :B, 2, sp), (i1+1, i2  , :B, 2, sp), b1,  λ)
      addhop((i1, i2, :B, 2, sp), (i1  , i2  , :B, 1, sp), b2,  λ)
      addhop((i1, i2, :B, 2, sp), (i1  , i2-1, :B, 1, sp), b3,  λ)
    end
  end

  if abs(U) > eps(Float64)
    #for idx in [1,2], i1 in 1:n1, i2 in 1:n2, orb in [:A, :B]
    for idx in [1,2], orb in [:A, :B], i2 in 1:n2, i1 in 1:n1
      addint((i1, i2, orb, idx, :UP), (i1, i2, orb, idx, :DN), a0, U)
    end
  end

  if abs(V) > eps(Float64)
    #for sp1 in [:UP, :DN], sp2 in [:UP, :DN], i1 in 1:n1, i2 in 1:n2
    for sp1 in [:UP, :DN], sp2 in [:UP, :DN], i2 in 1:n2, i1 in 1:n1
      addint((i1, i2, :A, 1, sp1), (i1  , i2  , :B, 1, sp2), a1, V)
      addint((i1, i2, :A, 1, sp1), (i1-1, i2  , :B, 2, sp2), a2, V)
      addint((i1, i2, :A, 1, sp1), (i1  , i2  , :B, 2, sp2), a3, V)

      addint((i1, i2, :A, 2, sp1), (i1  , i2+1, :B, 2, sp2), a1, V)
      addint((i1, i2, :A, 2, sp1), (i1  , i2  , :B, 1, sp2), a2, V)
      addint((i1, i2, :A, 2, sp1), (i1+1, i2  , :B, 1, sp2), a3, V)
    end
  end

  return hamspec
end
