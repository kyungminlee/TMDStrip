using JSON
using DataStructures
using ArgParse
import YAML

using ProgressMeter
using HartreeFockBogoliubov
import HartreeFockBogoliubov: Spec, Generator, HFB
using HartreeFockBogoliubov: HFB
using HartreeFockBogoliubov: Dictify

include("./HFBLooperPython.jl")


function makezigzagtmdhamiltonian(
        width ::Integer,
        μ ::Float64,
        t ::Float64,
        mAB ::Float64,
        λIsing ::Float64,
        U ::Float64, V ::Float64)
  a0 = [ 0.0, 0.0]
  a1 = [ 0.0, 1.0]
  a2 = [-sqrt(3.0) * 0.5,-0.5]
  a3 = [ sqrt(3.0) * 0.5,-0.5]
  b1 = a3 - a2
  b2 = a1 - a3
  b3 = a2 - a1

  c1 = b1 - b2
  c2 = b2 - b3

  unitcell = newunitcell([(b1*2) (b2*width)], OrbitalType=Tuple{Int64, Symbol, Int64, Symbol})
  orbloc = Dict(
    (:A, 1) => a1,
    (:B, 1) => -a2,
    (:A, 2) => a1+b1,
    (:B, 2) => -a2+b1,
  )

  for sp in [:UP, :DN], idx in [1,2], orb in [:A, :B], w in 1:width
    r = b2 * (w-1) + orbloc[orb, idx]
    addorbital!(unitcell, (w, orb, idx, sp), carte2fract(unitcell, r))
  end

  hamspec = Spec.FullHamiltonian(unitcell)

  function addhop(orb1, orb2, dr, value)
    if ! hasorbital(unitcell, orb1) || !hasorbital(unitcell, orb2)
      return
    end
    w = orb1[1]
    r1 = orbloc[orb1[2:3]] + b2 * (w-1)
    r2 = r1 + dr
    Spec.addhopping!(hamspec, Spec.hoppingbycarte(unitcell, value, orb1, orb2, r1, r2))
  end

  function addint(orb1, orb2, dr, value)
    if ! hasorbital(unitcell, orb1) || !hasorbital(unitcell, orb2)
      return
    end
    w = orb1[1]
    r1 = orbloc[orb1[2:3]] + b2 * (w-1)
    r2 = r1 + dr
    Spec.addinteraction!(hamspec, Spec.interactionbycarte(unitcell, value, orb1, orb2, r1, r2))
  end

  for sp in [:UP, :DN], w in 1:width, orb in [:A, :B], idx in [1,2]
    r = b2 * (w-1) + orbloc[orb, idx]
    val = (orb == :A) ? (-μ + mAB) : (-μ - mAB)
    Spec.addhopping!(hamspec, Spec.hoppingbycarte(unitcell, val, (w, orb, idx, sp), r))
  end

  for sp in [:UP, :DN], w in 1:width
    addhop( (w, :A, 1, sp), (w+1, :B, 1, sp), a1, -t)
    addhop( (w, :A, 1, sp), (w  , :B, 2, sp), a2, -t)
    addhop( (w, :A, 1, sp), (w  , :B, 1, sp), a3, -t)

    addhop( (w, :A, 2, sp), (w+1, :B, 2, sp), a1, -t)
    addhop( (w, :A, 2, sp), (w  , :B, 1, sp), a2, -t)
    addhop( (w, :A, 2, sp), (w  , :B, 2, sp), a3, -t)
  end

  if abs(λIsing) > eps(Float64)
    for sp in [:UP, :DN], w in 1:width, orb in [:A, :B]
      λ = 1im * λIsing * (sp == :UP ? 1 : -1) * (orb == :A ? 1 : -1)
      addhop( (w, orb, 1, sp), (w  , orb, 2, sp), b1, -λ)
      addhop( (w, orb, 1, sp), (w+1, orb, 1, sp), b2, -λ)
      addhop( (w, orb, 1, sp), (w-1, orb, 2, sp), b3, -λ)

      addhop( (w, orb, 2, sp), (w  , orb, 1, sp), b1, -λ)
      addhop( (w, orb, 2, sp), (w+1, orb, 2, sp), b2, -λ)
      addhop( (w, orb, 2, sp), (w-1, orb, 1, sp), b3, -λ)
    end
  end

  if abs(U) > eps(Float64)
    for idx in [1,2], w in 1:width, orb in [:A, :B]
      addint( (w, orb, idx, :UP), (w, orb, idx, :DN), a0, U )
    end
  end

  if abs(V) > eps(Float64)
    for sp1 in [:UP, :DN], sp2 in [:UP, :DN], w in 1:width
      addint((w, :A, 1, sp1), (w+1, :B, 1, sp2), a1, V)
      addint((w, :A, 1, sp1), (w  , :B, 2, sp2), a2, V)
      addint((w, :A, 1, sp1), (w  , :B, 1, sp2), a3, V)

      addint((w, :A, 2, sp1), (w+1, :B, 2, sp2), a1, V)
      addint((w, :A, 2, sp1), (w  , :B, 1, sp2), a2, V)
      addint((w, :A, 2, sp1), (w  , :B, 2, sp2), a3, V)
    end
  end

  return hamspec
end


function runzigzag(
  n1 ::Integer, n2 ::Integer, width::Integer;
  μ ::Real=0.0,
  t ::Real=1.0,
  mAB ::Real=0.0,
  λIsing ::Real=0.0,
  U ::Real=0.0,
  V ::Real=0.0,
  temperature ::Real=0.0,
  outpath::AbstractString="out",
  verbose::Bool=false)

  hamspec = makezigzagtmdhamiltonian(width, float(μ), float(t), float(mAB), float(λIsing), float(U), float(V))
  solver = HFBSolver(hamspec, [n1, n2], float(temperature))
  uc = hamspec.unitcell

  mkpath(outpath)
  open(joinpath(outpath, "parameters.yaml"), "w") do file
    FMT(x...) = begin
      foreach(z -> mydump(file, z), x)
      println(file)
    end
    FMT("---")
    FMT("Parameters:")
    FMT("  ChemicalPotential: ", μ)
    FMT("  NearestNeighborHopping: ", t)
    FMT("  ChargeTransferEnergy: ", mAB)
    FMT("  IsingSpinOrbitCoupling: ", λIsing)
    FMT("  OnSiteInteraction: ", U)
    FMT("  NearestNeighborInteraction: ", V)
    FMT("  Temperature: ", temperature)
    FMT("  SystemSize: ", [n1, n2])
    FMT("  Width: ", width)
    dumpall(file, uc)
    dumpall(file, solver)
    FMT("...")
  end

  open(joinpath(outpath, "hamiltonian.json"), "w") do file
    hamdict = dictify(hamspec)
    JSON.print(file, hamdict)
  end

  open(joinpath(outpath, "momentumgrid.json"), "w") do file
    JSON.print(file, solver.momentumgrid)
  end

  open(joinpath(outpath, "hfbsolver.json"), "w") do file
    hamdict = dictify(solver)
    JSON.print(file, hamdict)
  end

  open(joinpath(outpath, "hfbcomputer.json"), "w") do file
    hamdict = dictify(solver.hfbcomputer)
    JSON.print(file, hamdict)
  end

  function nogammaupdate(sol::HFBSolution, newsol::HFBSolution)
    simpleupdate(sol, newsol)
    sol.Γ[:] = 0
    sol
  end

  runloop(solver; outpath=outpath, nwarmup=100, nbunch=100, nbatch=100, verbose=verbose, update=nogammaupdate)
end



function eigenzigzag(
  n1 ::Integer, n2 ::Integer, width::Integer;
  μ ::Real=0.0,
  t ::Real=1.0,
  mAB ::Real=0.0,
  λIsing ::Real=0.0,
  U ::Real=0.0,
  V ::Real=0.0,
  temperature ::Real=0.0,
  compute_eigenvectors::Bool=false,
  outpath::AbstractString="out",
  verbose::Bool=false)

  hamspec = makezigzagtmdhamiltonian(width, float(μ), float(t), float(mAB), float(λIsing), float(U), float(V))
  solver = HFBSolver(hamspec, [n1, n2], float(temperature))
  uc = hamspec.unitcell

  currentsolution = detectresult(outpath)
  if currentsolution == nothing
    println("Bad solution. Quitting")
    return
  end

  hamiltonian = makehamiltonian(solver.hfbcomputer,
                                currentsolution.Γ,
                                currentsolution.Δ)
  if verbose
    println("Computing eigenvalues and eigenvectors")
  end

  all_eigenvalues = []
  all_eigenvectors = []
  minimum_eigenvalue = Inf64

  if verbose
    @showprogress for momentum in solver.momentumgrid
      hk = hamiltonian(momentum)
      if compute_eigenvectors
        (eigenvalues, eigenvectors) = eig(Hermitian(hk))
        push!(all_eigenvalues, eigenvalues)
        push!(all_eigenvectors, eigenvectors)
      else
        eigenvalues = eigvals(Hermitian(hk))
        push!(all_eigenvalues, eigenvalues)
      end

    end
  else
    for momentum in solver.momentumgrid
      hk = hamiltonian(momentum)
      if compute_eigenvectors
        (eigenvalues, eigenvectors) = eig(Hermitian(hk))
        push!(all_eigenvalues, eigenvalues)
        push!(all_eigenvectors, eigenvectors)
      else
        eigenvalues = eigvals(Hermitian(hk))
        push!(all_eigenvalues, eigenvalues)
      end
    end
  end
  open(joinpath(outpath, "eigenvalues.json"), "w") do file
    JSON.print(file, OrderedDict(
       "momentums" => solver.momentumgrid,
       "eigenvalues" => all_eigenvalues,
    ))
  end
  if compute_eigenvectors
    open(joinpath(outpath, "eigenvectors.json"), "w") do file
      JSON.print(file, OrderedDict(
         "momentums" => solver.momentumgrid,
         "eigenvalues" => all_eigenvalues,
         "eigenvectors" => all_eigenvectors,
      ))
    end
  end
end


function bareeigenzigzag(
  n1 ::Integer, n2 ::Integer, width::Integer;
  μ ::Real=0.0,
  t ::Real=1.0,
  mAB ::Real=0.0,
  λIsing ::Real=0.0,
  compute_eigenvectors::Bool=false,
  outpath::AbstractString="out",
  verbose::Bool=false)

  mkpath(outpath)

  hamspec = makearmchairtmdhamiltonian(width, float(μ), float(t), float(mAB), float(λIsing), 0.0, 0.0)
  solver = HFBSolver(hamspec, [n1, n2], 0.0)
  uc = hamspec.unitcell

  hamiltonian = makehoppingmatrix(solver.hfbcomputer)

  if verbose
    println("Computing eigenvalues and eigenvectors")
  end

  all_eigenvalues = []
  all_eigenvectors = []
  minimum_eigenvalue = Inf64

  if verbose
    @showprogress for momentum in solver.momentumgrid
      hk = hamiltonian(momentum)
      if compute_eigenvectors
        (eigenvalues, eigenvectors) = eig(Hermitian(hk))
        push!(all_eigenvalues, eigenvalues)
        push!(all_eigenvectors, eigenvectors)
        minimum_eigenvalue = min(minimum_eigenvalue, minimum(abs.(eigenvalues)))
      else
        eigenvalues = eigvals(Hermitian(hk))
        push!(all_eigenvalues, eigenvalues)
        minimum_eigenvalue = min(minimum_eigenvalue, minimum(abs.(eigenvalues)))
      end
    end
  else
    for momentum in solver.momentumgrid
      hk = hamiltonian(momentum)
      if compute_eigenvectors
        (eigenvalues, eigenvectors) = eig(Hermitian(hk))
        push!(all_eigenvalues, eigenvalues)
        push!(all_eigenvectors, eigenvectors)
        minimum_eigenvalue = min(minimum_eigenvalue, minimum(abs.(eigenvalues)))
      else
        eigenvalues = eigvals(Hermitian(hk))
        push!(all_eigenvalues, eigenvalues)
        minimum_eigenvalue = min(minimum_eigenvalue, minimum(abs.(eigenvalues)))
      end
    end
  end
  open(joinpath(outpath, "bare_eigenvalues.json"), "w") do file
    JSON.print(file, OrderedDict(
       "momentums" => solver.momentumgrid,
       "eigenvalues" => all_eigenvalues,
    ))
  end
  if compute_eigenvectors
    open(joinpath(outpath, "bare_eigenvectors.json"), "w") do file
      JSON.print(file, OrderedDict(
         "momentums" => solver.momentumgrid,
         "eigenvalues" => all_eigenvalues,
         "eigenvectors" => all_eigenvectors,
      ))
    end
  end
end


function parse_commandline()
  s = ArgParseSettings()
  @add_arg_table s begin
    "infilename"
    help = "Input Filename"
    required = true
    "--eigenvalues"
    help = "Compute Eigenvalues"
    action = :store_true
    "--eigenvectors"
    help = "Compute Eigenvectors"
    action = :store_true
    "--bare"
    help = "Compute Bare Bandstructure"
    action = :store_true
    "--verbose", "-v"
    action = :store_true
    "--n1"
    arg_type = Int
    default = 0
    "--n2"
    arg_type = Int
    default = 0
  end
  return parse_args(s)
end

function main()
  args = parse_commandline()

  parameters = YAML.load(open(args["infilename"]))

  boundarytype = parameters["BoundaryType"]
  @assert(boundarytype == "Zigzag")

  outpath = parameters["OutPath"]
  n1 = parameters["n1"]
  n2 = parameters["n2"]
  width = parameters["Width"]
  μ = parameters["ChemicalPotential"]
  t = parameters["NearestNeighborHopping"]
  mAB = parameters["ChargeTransferEnergy"]
  λIsing = parameters["IsingSpinOrbitCoupling"]
  U = parameters["OnSiteInteraction"]
  V = parameters["NearestNeighborInteraction"]
  temperature = parameters["Temperature"]
  seed = if haskey(parameters, "Seed")
    Int(parameters["Seed"])
  else
    Int(time_ns())
  end

  srand(seed)
  n1 = (args["n1"] > 0) ? args["n1"] : n1
  n2 = (args["n2"] > 0) ? args["n2"] : n2

  if args["bare"]
    bareeigenzigzag(n1, n2, width;
           μ=μ,
           t=t,
           mAB=mAB,
           λIsing=λIsing,
           outpath=outpath,
           verbose=args["verbose"])
  elseif args["eigenvalues"] || args["eigenvectors"]
    eigenzigzag(n1, n2, width;
           μ=μ,
           t=t,
           mAB=mAB,
           λIsing=λIsing,
           U=U,
           V=V,
           temperature=temperature,
           compute_eigenvectors=args["eigenvectors"],
           outpath=outpath,
           verbose=args["verbose"])

  else
    runzigzag(n1, n2, width;
           μ=μ,
           t=t,
           mAB=mAB,
           λIsing=λIsing,
           U=U,
           V=V,
           temperature=temperature,
           outpath=outpath,
           verbose=args["verbose"])
  end
end

main()
