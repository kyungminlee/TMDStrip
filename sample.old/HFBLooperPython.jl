using JSON
using DataStructures
using ArgParse
import YAML

using ProgressMeter
using HartreeFockBogoliubov
import HartreeFockBogoliubov: Spec, Generator, HFB
using HartreeFockBogoliubov: HFB


function detectresult(outpath::AbstractString)
  for resultfilename in ["result_concise.json", "result_history.json"]
    result = nothing
    resultfilepath = joinpath(outpath, resultfilename)
    if isfile(resultfilepath)
      open(resultfilepath) do file
        result = JSON.load(file)
      end
    end
    if result != nothing
      ρ = [ float(x["re"]) + 1im * float(x["im"]) for x in result["rho"] ]
      t = [ float(x["re"]) + 1im * float(x["im"]) for x in result["t"] ]
      Γ = [ float(x["re"]) + 1im * float(x["im"]) for x in result["Gamma"] ]
      Δ = [ float(x["re"]) + 1im * float(x["im"]) for x in result["Delta"] ]
      return HFBSolution(ρ,t,Γ,Δ)
    end
  end

  for resultfilename in ["result_concise.yaml", "result_history.yaml"]
    result = nothing
    resultfilepath = joinpath(outpath, resultfilename)
    if isfile(resultfilepath)
      open(resultfilepath) do file
        prev_yaml = YAML.load_all(file)
        for s in prev_yaml
          result = s
        end
      end
    end

    if result != nothing
      ρ = [ float(x["real"]) + 1im * float(x["imag"]) for x in result["rho"] ]
      t = [ float(x["real"]) + 1im * float(x["imag"]) for x in result["t"] ]
      Γ = [ float(x["real"]) + 1im * float(x["imag"]) for x in result["Gamma"] ]
      Δ = [ float(x["real"]) + 1im * float(x["imag"]) for x in result["Delta"] ]
      return HFBSolution(ρ,t,Γ,Δ)
    end
  end
  return nothing
end

function runloop(solver ::HFBSolver;
                 outpath ::AbstractString="out",
                 nwarmup ::Integer = 200,
                 nbunch  ::Integer = 100,
                 nbatch  ::Integer = 500,
                 tolerance::Float64 = sqrt(eps(Float64)),
                 verbose::Bool=false,
                 update::Function=simpleupdate)
  assert(nwarmup >= 0)
  assert(nbunch > 0)
  assert(nbatch >= 0)
  mkpath(outpath)

  hamspec = solver.hamiltonian
  uc = hamspec.unitcell

  currentsolution = newhfbsolution(solver.hfbcomputer)
  lastsolution = detectresult(outpath)
  if lastsolution != nothing
    if iscompatible(currentsolution, lastsolution)
      println("Previous Solution matches in size with current solution")
      println("Let's use it")
      currentsolution = lastsolution
      println("Successfully read.")

      if verbose
        !isempty(currentsolution.ρ) && println("  maximum |rho|  : ", maximum(abs.(currentsolution.ρ)))
        !isempty(currentsolution.t) && println("  maximum |t|    : ", maximum(abs.(currentsolution.t)))
        !isempty(currentsolution.Γ) && println("  maximum |Gamma|: ", maximum(abs.(currentsolution.Γ)))
        !isempty(currentsolution.Δ) && println("  maximum |Delta|: ", maximum(abs.(currentsolution.Δ)))
      end
    else
      println("Previous solution has different size from current solution")
      println("Something is wrong. Just using the new solution")
    end
  else
    randomize!(solver.hfbcomputer, currentsolution)
  end

  let 
    sol1 = getnextsolutionpython(solver, currentsolution)
    sol2 = getnextsolution(solver, currentsolution)
    @show sol1
    @show sol2
    @show maximum(abs.(sol1.ρ - sol2.ρ) )
    @show maximum(abs.(sol1.t - sol2.t) )
    @show maximum(abs.(sol1.Γ - sol2.Γ) )
    @show maximum(abs.(sol1.Δ - sol2.Δ) )
    exit(1)
  end
  println("Warm Up")
  if verbose
    @showprogress for run in 1:nwarmup
      currentsolution = getnextsolutionpython(solver, currentsolution)
      currentsolution.Γ[:] = 0
    end
  else
    for run in 1:nwarmup
      currentsolution = getnextsolutionpython(solver, currentsolution)
      currentsolution.Γ[:] = 0
    end
  end

  for batchrun in 1:nbatch
    verbose && println("BATCH $batchrun")
    starttime = now()
    previoussolution = copy(currentsolution)
    currentsolution = looppython(solver, currentsolution, nbunch;
                           update=update, progressbar=verbose)
    endtime = now()
    verbose && println("Duration: ", (endtime - starttime))

    maxdiff1 = isempty(currentsolution.ρ) ? 0.0 : maximum(abs.(currentsolution.ρ - previoussolution.ρ))
    maxdiff2 = isempty(currentsolution.t) ? 0.0 : maximum(abs.(currentsolution.t - previoussolution.t))
    maxdiff = max(maxdiff1, maxdiff2)

    if verbose
      println("MaxDiff_rho = $maxdiff1")
      println("MaxDiff_t   = $maxdiff2")
      println("MaxDiff     = $maxdiff")
    end
    open(joinpath(outpath, "result_history.yaml"), "a") do file
      FMT(x...) = begin
        foreach(z -> mydump(file, z), x)
        println(file)
      end
      FMT("---")
      FMT("BatchRun: ",    batchrun)
      FMT("rho: ",         currentsolution.ρ)
      FMT("t: ",           currentsolution.t)
      FMT("Gamma: ",       currentsolution.Γ)
      FMT("Delta: ",       currentsolution.Δ)
      FMT("MaxDiff_rho: ", maxdiff1)
      FMT("MaxDiff_t: ",   maxdiff2)
      FMT("StartTime: ",   Dates.format(starttime, "yyyy-mm-ddTHH:MM:SS.s"))
      FMT("EndTime: ",     Dates.format(endtime, "yyyy-mm-ddTHH:MM:SS.s"))
      FMT("...")
    end

    open(joinpath(outpath, "result_concise.yaml"), "w") do file
      FMT(x...) = begin
        foreach(z -> mydump(file, z), x)
        println(file)
      end
      FMT("---")
      FMT("BatchRun: ",    batchrun)
      FMT("rho: ",         currentsolution.ρ)
      FMT("t: ",           currentsolution.t)
      FMT("Gamma: ",       currentsolution.Γ)
      FMT("Delta: ",       currentsolution.Δ)
      FMT("MaxDiff_rho: ", maxdiff1)
      FMT("MaxDiff_t: ",   maxdiff2)
      FMT("StartTime: ",   Dates.format(starttime, "yyyy-mm-ddTHH:MM:SS.s"))
      FMT("EndTime: ",     Dates.format(endtime, "yyyy-mm-ddTHH:MM:SS.s"))
      FMT("...")
    end

    if maxdiff < tolerance
      return
    end

  end
end
